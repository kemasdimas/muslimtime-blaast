var LinearLayout = require('ui').LinearLayout;
exports.TimeBar = LinearLayout.extend({
	scene: {},
	layers: {
		background: 0,
		timeLabel: 1,
		timeString: 2
	},
	
	initialize: function(timeName, timeString, backgroundName, backgroundNameFocused) {
		exports.TimeBar.__super__.initialize.call(this);
		
		var TextView = require('ui').TextView;
		var SceneView = require('ui').SceneView;
		
		this.scene = new SceneView({
			style: {
				width: 'fill-parent',
				height: 36
			}
		});
		this.scene.setLayers(3);
		
		var label = new TextView({
			label: menuName,
			style: {
				border: '8',
				'background-color': 'transparent',
				color: 'black',
				'font-size': 'large'
			}
		});
	
		var time = new TextView({
			label: timeString,
			style: {
				border: '8',
				'background-color': 'transparent',
				color: 'black',
				'font-size': 'large'
			}
		});
	
		// 2. Use setLayerControl to assign control to the layer
		this.scene.setLayerControl(this.layers.timeLabel, label);
		this.scene.translate(this.layers.timeLabel, 42, 0);
		
		this.scene.setLayerControl(this.layers.timeString, time);
		this.scene.translate(this.layers.timeLabel, 200, 0);
	}
});

exports.IconedMenu = function(menuName, timeString, backgroundName, backgroundNameFocus){
	var TextView = require('ui').TextView;
	var SceneView = require('ui').SceneView;
	var barHeight = 60;
	
	var scene = new SceneView({
		name: menuName,
		style: {
			width: 'fill-parent', 
			height: barHeight
		}
	});
	/**
	 * 0: background focused
	 * 1: background infocused *disabled!
	 * 2: name label
	 * 3: time label
	 */
	scene.setLayers( 4 );

	var label = new TextView({
		label: menuName,
		style: {
			border: '2',
			'background-color': 'black',
			color: 'white',
			'font-size': 'large',
			height: 'wrap-content',
			valign: 'middle'
		}
	});
	label.on('resized', function(w, h) {
		offsetY = (barHeight / 2) - (h / 2);
		
		scene.translate(2, 20, offsetY);
	});

	var time = new TextView({
		label: timeString,
		style: {
			border: '2',
			'background-color': 'black',
			align: 'right',
			color: 'white',
			'font-size': 'large',
			height: 'wrap-content',
			valign: 'middle'
		}
	});
	time.on('resized', function(w, h) {
		offsetX = scene.dimensions().width - w - 20;
		offsetY = (barHeight / 2) - (h / 2);
		
		scene.translate(3, offsetX, offsetY);
	});

	scene.on('hilite', function() {
		scene.changeLayer(1, {
			visible: false
		});
	});
	scene.on('unhilite', function() {
		scene.changeLayer(1, {
			visible: true
		});
	});
	scene.on('focus', function() {
		label.style({
			'background-color': '#666'
		});
		time.style({
			'background-color': '#666'
		});
	});
	scene.on('blur', function() {
		label.style({
			'background-color': '#000'
		});
		time.style({
			'background-color': '#000'
		});
	});
	
	// 2. Use setLayerControl to assign control to the layer
	scene.setLayerControl(2, label);
	scene.setLayerControl(3, time);
	scene.on('resized', function(width, height) {
		// console.log('Bar resized: ' + width);
		
		// scene.defineSpritesheet('icon', app.imageURL( backgroundName ), width, barHeight);
	    scene.defineSpritesheet('icon2', app.imageURL( backgroundNameFocus ), width, barHeight);
	    scene.add({
			sprite: 'icon2',
			x: 0,
			y: 0,
			layer: 0,
			frame: 0
		});
	    // scene.add({
			// sprite: 'icon',
			// x: 0,
			// y: 0,
			// layer: 1,
			// frame: 0
		// });
	});
	
	return scene;
};