exports.DB = [
	{
		title: 'Fajr',
		image: 'fajr.png',
		imageHighlight: 'fajrh.png',
		id: 'fajr'
	},
	{
		title: 'Sunrise',
		image: 'sunrise.png',
		imageHighlight: 'sunriseh.png',
		id: 'sunrise'	
	},
	{
		title: 'Zuhr',
		image: 'zuhr.png',
		imageHighlight: 'zuhrh.png',
		id: 'zuhr'	
	},
	{
		title: 'Ashr',
		image: 'asr.png',
		imageHighlight: 'asrh.png',
		id: 'asr'	
	},
	{
		title: 'Maghrib',
		image: 'maghrib.png',
		imageHighlight: 'maghribh.png',
		id: 'maghrib'	
	},
	{
		title: 'Isyaa\'',
		image: 'isha.png',
		imageHighlight: 'ishah.png',
		id: 'isha'	
	}
];
