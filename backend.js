// Clients list
var clients = {};

var storage = require('blaast/simple-data');
var ReqLog  = require('blaast/mark').RequestLogger;
var Scaling = require('blaast/scaling').Scaling;
var ltx = require('ltx');
var QS = require('querystring');
var _  = require('underscore');
var http = require('blaast/simple-http');
var sys = require('sys');
var rlog = new ReqLog(app.log);
var scaling = new Scaling(app.config);

var geocodeUrl = 'http://maps.googleapis.com/maps/api/geocode/json';
var earthToolsUrl = 'http://www.earthtools.org/timezone-1.1';
var xhanchApi = 'http://xhanch.com/api/islamic-get-prayer-time.php';

function PrayerTimeApi() {
}
PrayerTimeApi.prototype = {
	request: function(requestId, url, params, cb) {
		var r = rlog.start(requestId);
		params.m = 'json';
		
		url = xhanchApi + url + '?' + QS.stringify(params);
		console.log('Request url: ' + url);
		http.get(url, {
			ok: function(data) {
				r.done();
				cb(requestId, JSON.parse(data));
			},
			error: function(err) {
				r.error(err);
				cb(requestId, err);
			}
		});
	},
	
	getPrayerTime: function(lat, lng, year, month, offset, cb) {
		this.request('address', '', {
			lng: lng,
			lat: lat,
			yy: year,
			mm: month,
			gmt: offset
		}, cb);
	}
};

function EarthToolsAPI() {
}
EarthToolsAPI.prototype = {
	request: function(requestId, url, params, cb) {
		var r = rlog.start(requestId);
		
		url = earthToolsUrl + url;
		_.forEach(params, function(value, name) {
			url = url + '/' + value;
		});
		
		console.log('Request url: ' + url);
		http.get(url, {
			ok: function(data) {
				r.done();
				cb(requestId, {err: null, result: data});
			},
			error: function(err) {
				r.error(err);
				cb(requestId, {err: 'EarthToolsAPI error', result: null});
			}
		});
	},
	
	getTimezone: function(lat, lng, cb) {
		this.request('address', '', {lat: lat, lng: lng}, function(requestId, data) {
			if (data.err === null) {
				var xml = ltx.parse(data.result);
				
				cb(requestId, {err: null, result: xml.children[8].children[0]});
			} else {
				cb(requestId, {err: data.err, result: null});
			}
		});
	}
};

function GoogleGeocodeAPI() {
}
GoogleGeocodeAPI.prototype = {
	request: function(requestId, url, params, cb) {
		var r = rlog.start(requestId);
		
		url = geocodeUrl + url + '?' + QS.stringify(params);
		console.log('Request url: ' + url);
		http.get(url, {
			ok: function(data) {
				r.done();
				cb(requestId, JSON.parse(data));
			},
			error: function(err) {
				r.error(err);
				cb(requestId, err);
			}
		});
	},
	
	getLatLng: function(address, cb) {
		this.request('address', '', {address: address, sensor: false}, cb);
	}
};

function MuslimTimeUser(client) {
	this.client = client;
	this.geocodeApi = new GoogleGeocodeAPI();
	this.earthToolsApi = new EarthToolsAPI();
	this.prayerTimeApi = new PrayerTimeApi();
}
MuslimTimeUser.prototype = {
	setLocation: function(userLocation) {
		var self = this;
		
		self.geocodeApi.getLatLng(userLocation, function(requestId, data) {
			if (data.status !== null && data.status === 'OK') {
				try {
					var latLng = data.results[0].geometry.location;
					self.saveData(self.client.user.id, {
						userLocation: userLocation,
						userLatLng: {
							lat: Math.round(latLng.lat * 100)/100,
							lng: Math.round(latLng.lng * 100)/100
						}
					}, function(err, result) {
						self.setTimezone(null, {
							lat: null, 
							lng: null, 
							callback: function(err, data, scope) {
								self.setPrayerTime(err, {
									callback: function(err, data, scope) {
										self.client.msg('syncPrayerTime', 
											{ err: null, result: self.convertPrayerTime(data.prayerTime) });
										self.client.msg('prayerTimeFetched', 
											{ err: null, result: self.convertPrayerTime(data.prayerTime) });
									}
								}, scope);
							}
						}, self);
						self.client.msg('setLocation', { result: true });
					});
				} catch(err) {
					log.error('Failed: ' + err);
					self.client.msg('setLocation', { result: false });
				}
			}
		});
	},
	
	setTimezone: function(err, data, scope) {
		var self = scope;
		var ownData = data;
		
		// If lat / lng is not defined, fetch from database
		if (data.lat === undefined || data.lng === undefined || data.lat === null || data.lng === null) {			
			storage.get(self.client.user.id, function(err, data) {				
				self.setTimezone(err, {
					lat: data.userLatLng.lat, 
					lng: data.userLatLng.lng, 
					callback: ownData.callback
				}, self);
			});
		} else {
			self.earthToolsApi.getTimezone(data.lat, data.lng, function(requestId, data) {
				if (data.err !== undefined && data.err !== null) {
					if (ownData.callback !== undefined) {
						ownData.callback(err, null, self);
					}
				} else {
					self.saveData(self.client.user.id, { offset: data.result }, function(err, result) {
						if (result) {
							self.client.msg('syncTimezone', { err: err, result: data.result });
							
							if (ownData.callback !== undefined) {
								ownData.callback(null, null, self);
							}
						}
					});	
				}
			});
		}
	},
	
	// Function to comply with timezone API terms
	getTimezone: function(err, data, scope) {
		var self = scope;
		var ownData = data;
		
		storage.get(self.client.user.id, function(err, data) {	
			var offset = 0;
			err = 'No timezone defined.';
			if (data !== null && data.offset !== undefined && data.offset !== null) {
				err = null;
				offset = data.offset;
			}
			
			if (ownData.callback !== undefined) {
				ownData.callback(err, { offset: offset });
			}
		});
	},
	
	setPrayerTime: function(err, data, scope) {
		var self = scope;
		var ownData = data;
		
		if (err === null) {
			storage.get(self.client.user.id, function(err, data) {	
				var now = new Date();
				
				// Adjusting the offset!
				now.setHours(now.getHours() + data.offset);
				
				self.prayerTimeApi.getPrayerTime(data.userLatLng.lat, data.userLatLng.lng, 
					now.getFullYear(), now.getMonth() + 1, data.offset, function(requestId, data) {
						
						// Save to database for resync
						self.saveData(self.client.user.id, { prayerTime: data }, function(err, result) {
							self.setLastSyncTime(err, {
								syncTime: (new Date()).getTime().toString(),
								callback: function(err, result) {
									if (err === null) {
										// console.dir(result);
										if (ownData.callback !== undefined) {
											ownData.callback(err, result, self);
										}
									}
								}
							}, self);
						});
					});
			});
		} else {
			if (ownData.callback !== undefined) {
				ownData.callback(err, null, self);
			}
		}
	},
	
	getPrayerTime: function(err, data, scope) {
		var self = scope;
		var ownData = data;
		
		// TODO: Jika bulan sama, cukup ambil dari dbase
		// Jika bulan beda, ambil lagi dari webservice!
		storage.get(self.client.user.id, function(err, data) {
			if (data !== null && data.prayerTime !== undefined && data.prayerTime !== null) {
				
				if (!self.isDifferentMonth(Number(ownData.syncTime), Number(data.lastSync), data.offset)) {
					if (ownData.callback !== undefined) {
						ownData.callback(null, self.convertPrayerTime(data.prayerTime));
					}
				} 
				else {
					console.log('Resync!');
					self.setPrayerTime(err, {
						callback: function(err, data, scope) {
							ownData.callback(null, self.convertPrayerTime(data.prayerTime));
						}
					}, scope);
				}
			} else {
				if (ownData.callback !== undefined) {
					ownData.callback(null, [ ]);
				}
			}
		});
	},
	
	convertPrayerTime: function(prayerTime) {
		var temp = [];
		if (prayerTime !== null) {
			_.forEach(prayerTime, function(value, name) {
				if (name !== '_id' && name !== '_rev') {
					temp.push(value);
				}
			});
		}
		
		return temp;
	},
	
	isDifferentMonth: function(timeNow, storedTime, offset) {
		if (!isNaN(storedTime)) {
			console.log(timeNow + '::' + storedTime);
			
			var dateNow = new Date(timeNow);
			var dateStored = new Date(storedTime);
			
			dateNow.setHours(dateNow.getHours() + offset);
			dateStored.setHours(dateStored.getHours() + offset);
			
			console.log(dateNow);
			console.log(dateStored);
			
			if (dateNow.getMonth() === dateStored.getMonth() && 
					dateNow.getYear() === dateStored.getYear()) {
						
				return false;	
			}
		}
		
		return true;
	},
	
	setLastSyncTime: function(err, data, scope) {
		var self = scope;
		
		self.saveData(self.client.user.id, { lastSync: data.syncTime }, data.callback);
	},
	
	saveData: function(key, newData, callback) {
		storage.get(key, function(err, data) {
			// console.dir(data);
			if (data === null) {
				data = {};
			}
			_.extend(data, newData);		
			// console.dir(data);
			
			storage.set(key, data, function(err, result) {
				callback(err, data);
			});
		});
	}
};

app.realtime(function(client, event) {
	if (event === 'CONNECTED') {
		clients[client.id] = client;
	} else if (event === 'DISCONNECTED') {
		log.info(client.id + ' disconnected.');
		delete clients[client.id];
	}
});

app.message(function(client, action, data) {
	var user;
	if (action === 'setLocation') {
		user = new MuslimTimeUser(client);
		
		user.setLocation(data.userLocation);
	} else if (action === 'sync') {
		user = new MuslimTimeUser(client);
		
		user.getTimezone(null, {
			callback: function(err, result) {
				user.client.msg('syncTimezone', { err: err, result: result.offset });
			}
		}, user);
		
		user.getPrayerTime(null, {
			syncTime: data.syncTime, 
			callback: function(err, result) {
				user.client.msg('syncPrayerTime', { err: err, result: result });
			}
		}, user);
	}
});
