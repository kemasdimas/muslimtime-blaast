// HelloBlaast -- bootstrap.js
var app = this;
var storage = app.storage('MuslimTime');
var _ = require('common/util');

app.on('connected', function() {
	// Sync monthly prayer times with the server when view loaded
	app.msg('sync', { syncTime: app.serverTime().toString() });
	
	app.on('message', function(action, data) {
		if (action === 'syncPrayerTime') {
			if (data.err === null) {
				var location = storage.get('userLocation').location;
				if (location !== undefined) {
					storage.set('prayerTime', { prayerTime: data.result });
				}
			} else {
				console.log('Prayer time sync error: ' + data.err);
			}
		} else if (action === 'syncTimezone') {
			if (data.err === null) {				
				storage.set('timezone', { offset: data.result });
			} else {
				console.log('Timezone Sync error: ' + data.err);
			}
		}
	});
});

app.on('disconnected', function() {
	console.log('Disconnected from backend.');
});