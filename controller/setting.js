var app = this;
var _ = require('common/util');
var customUi = require('../lib/LoaderView');
var IslamicDate = require('../lib/IslamicDate');
var TextView = require('ui').TextView;
var storage = app.storage('MuslimTime');

var loader;
_.extend(exports, {
	':initialize': function(args) {
		this.parentPanels = args[0];
	},
	
	':active': function() {
		var view = this;		
		view.index = undefined;

		var txtLocation = view.get('body').get('txtLocation');
		var txtHijriAdjustment = view.get('body').get('txtHijriAdjustment');

		var location = storage.get('userLocation').location;
		if (location !== undefined) {
			txtLocation.value(location);
		}

		var adjustment = storage.get('hijriDate').adjustment;
		if (adjustment !== undefined) {
			txtHijriAdjustment.value(adjustment);
		}
	},
	
	':inactive': function() {
		
	},
	
	':keypress': function(key) {
		var view = this;
		
		if (view.controls.length > 0 && (key === 'down' || key === 'up' || key === 'fire')) {
			var prevIndex = view.index;
			
			if (view.index === undefined) {
				prevIndex = 0;
				view.index = 0;
			} else if (key === 'down' && view.index < view.controls.length - 1) {
				view.index++;
			} else if (key === 'up' && view.index > 0) {
				view.index--;
			}
			
			view.controls[view.index].emit('keypress', 'fire');
		}
	},
	
	inputFocus: function(input) {
		input.style({
			'background-color': '#dedede',
			color: 'black'
		});
	},
	
	inputBlur: function(input) {
		input.style({
			'background-color': '#4293D0',
			color: 'white'
		});
	},
	
	':load': function() {		
		var view = this;
		var txtLocation = view.get('body').get('txtLocation');
		var txtHijriAdjustment = view.get('body').get('txtHijriAdjustment');
		
		view.controls = [];
		view.controls.push(txtLocation);
		view.controls.push(txtHijriAdjustment);
		
		// Handling the blur / focus of textfields
		txtLocation.on('focus', function() {
			view.inputFocus(this);
		});
		txtLocation.on('blur', function() {
			view.inputBlur(this);
		});
		
		txtHijriAdjustment.on('focus', function() {
			view.inputFocus(this);
		});
		txtHijriAdjustment.on('blur', function() {
			view.inputBlur(this);
		});
		
		txtLocation.on('submit', function() {
			var text = txtLocation.value();
			
			if (text.length > 3) {
				var storedLocation = storage.get('userLocation').location;
				
				if (storedLocation !== text) {
					app.msg('setLocation', { userLocation: text });
					loader.startLoading();
				
					storage.set('userLocation', { location: text });
				}
				
				txtLocation.emit('blur');
				view.emit('keypress', 'down');
			}
		});
		txtHijriAdjustment.on('submit', function() {
			var text = txtHijriAdjustment.value();
			
			if (text.length > 0) {
				var storedAdjustment = storage.get('hijriDate').adjustment;
				var adjustment = parseInt(text, 10);
				
				if (storedAdjustment !== adjustment) {
					this.value(adjustment.toString());
					
					var islamicDate = new IslamicDate.IslamicDate();
					storage.set('hijriDate', { adjustment: adjustment, 
						date: islamicDate.getIslamicDate(adjustment) });
				}
			}
		});
		
		loader = new customUi.LoaderView('loading.png', 48, 12, 'Please wait...');
		// loader.stopLoading();
		
		// TODO: Ini harusnya dibalik!
		view.add(new TextView({
			name: "lbl3",
			label: "App. by KomodoDev",
			style: {
				align: "center",
				width: "fill-parent",
				height: "wrap-content",
				border: "5",
				'font-size': "small",
				color: "white"
			}
		}));
		view.add(loader);

		app.on('message', function(action, data) {
			if (action === 'prayerTimeFetched') {
				app.vibrate(50);
				loader.stopLoading();
			}
		});
	}
});
