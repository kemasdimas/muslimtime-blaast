var _ = require('common/util');
var TextView = require('ui').TextView;
var db = require('../lib/DB').DB;
var iconedMenu = require('../lib/IconedMenu');

var app = this;
var storage = app.storage('MuslimTime');

// When a view is used under Panels, events 'focus' and 'blur' are sent to indicate
// switching in and out of the page.
_.extend(exports, {
	initialize: function(args) {
		this.parentPanels = args[0];
	},
	
	':active': function() {
		var view = this;
		var i = 0;
		var todayDate = (new Date(app.serverTime())).getDate();
		var todayPrayerTime = storage.get('prayerTime').prayerTime[todayDate - 1];
		view.clear();
		
		if (todayPrayerTime !== undefined && todayPrayerTime !== null) {
			activeTime = 0;
			view.add(new TextView({
				label: 'Location: ' + storage.get('userLocation').location,
				style: {
					'background-color': '#2B5D82',
					color: '#fff',
					border: '3 0 0',
					width: 'fill-parent',
					align: 'center',
					'font-size': 'small'
				}
			}));
			view.add(new TextView({
				label: storage.get('hijriDate').date,
				style: {
					'background-color': '#2B5D82',
					color: '#fff',
					border: '1 0 5',
					width: 'fill-parent',
					align: 'center',
					'font-size': 'small'
				}
			}));
			
			db.forEach(function(item){
				view.add( item.title, 
					iconedMenu.IconedMenu( item.title, todayPrayerTime[item.id], item.image, item.imageHighlight ) );
	            
	            view.get(i).emit('blur');
	            i++;
	        });
			view.focusItem(2);
			view.scrollTop(0);
		} else {	
			view.add(new TextView({
				label: 'Please set your location in setting tab ->',
				style: {
					color: 'white',
					border: '10 5',
					width: 'fill-parent',
					height: 'fill-parent',
					align: 'center',
					valign: 'middle',
					'font-size': 'large'
				}
			}));
			
			if (view.firstAppearance) {
				view.parentPanels.lookAt(2);
			}
			
			view.firstAppearance = false;
		}
	},
	
	':inactive': function() {
	},
	
	':keypress': function(key) {
		var view = this;
		var next;
		
		if (view.index === undefined) {
			if (view.size() > 1) {
				view.focusItem(1);
			}
		} else if (key === 'up' || key === 'down') {
			next = view.index + (key === 'up' ? -1 : 1);
			
			if (next < 2) {
				next = 2;
			} else if (next > (view.size() - 1)) {
				next = view.size() - 1;
			}
			
			if (view.index === next) {
				return;
			}
			
			view.focusItem(next);
		}
		/* 
		else if (key === '84' || key === '116') {
			next = 0;
			
			view.focusItem(next);
			view.selection.select(next);
		} else if (key === '66' || key === '98') {
			next = view.size() - 1;
			
			view.focusItem(next);
			view.selection.select(next);
		}*/
	},
	
	':load': function() {
		// console.log('First panel loaded');
		
		var view = this;
		var i = 0;
		var todayDate = (new Date(app.serverTime())).getDate();
		var todayPrayerTime = storage.get('prayerTime').prayerTime[todayDate - 1];
		
		view.firstAppearance = true;
	},
	
	hiliteItem: function(index) {
		var view = this;
		
		view.forEach(function(item) {
			item.emit('unhilite');
		});
		
		if (index >= 0) {
			view.get(index).emit('hilite');
		}
	},
	
	focusItem: function(index) {
		var view = this;
		
        if (view.index !== undefined) {
            view.get(view.index).emit('blur');
        }

        view.index = index;
        view.get(index).emit('focus');
        if(index === 2){
            view.scrollTop(0);
        }
        
        view.scrollTo(index);
    }
});
