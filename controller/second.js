var _ = require('common/util');

var app = this;
var IslamicDate = require('../lib/IslamicDate');
var storage = app.storage('MuslimTime');

// When a view is used under Panels, events 'focus' and 'blur' are sent to indicate
// switching in and out of the page.

_.extend(exports, {
	':initialize': function(args) {
		this.parentPanels = args[0];
	},
	
	':active': function() {
		var view = this;
		
		var date = storage.get('hijriDate').date;
		if (date !== undefined) {
			view.get('txtDate').label(date);
		}
	}
});
